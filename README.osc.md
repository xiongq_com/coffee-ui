<div align="center">
<h1>Coffee Admin</h1>
</div>

<p align="center">
    <img src="https://img.shields.io/badge/Spring%20Boot-2.5.8-blue" alt="Downloads">
    <img src="https://img.shields.io/badge/JDK-1.8+-green.svg" alt="Build Status">
    <img src="https://img.shields.io/badge/license-Apache%202-blue.svg" alt="Build Status">
</p>

### 简介

Coffee Admin 权限管理系统，基于 VUE3.x、SpringBoot2.x、Sa-Token、MyBatis-Plus 等技术实现的前后端分离的权限管理系统。 可用于学习参考和项目开发。

### 特性

- WEB 容器使用了 undertow，相较于 tomcat，并发性更好，性能要好一些。
- Lombok，消除冗长的 java 代码，更加简化。
- Mybatis Plus，可以简化 CRUD 开发。
- Mybatis Plus Generator，生成前后端代码，简化开发工作量。
- 使用 java 新特性，Stream API、lambda 表达式等。
- Hutool 工具集合，减少项目工具类的编写。
- Sa-Token 权限认证框架，细分到页面按钮级别。
- EasyExcel，方便导入导出功能，自定义 Convert 类，实现了数据字典的转化。
- Guava，非常方便的 java 工具集，提供了类似 Lists.newArrayList()和 Sets.newHashSet()等静态方法。
- DataSource 注解，支持多数据源切换。
- Fastjson，方便了 JSON 的格式化和解析。
- Alibaba Java Coding Guidelines 插件，IDEA 插件，提高代码质量。
- MinIO，分布式文件存储。
- 前端框架采用最新技术栈，Vue3 & Vite，打包更快更轻。
- 前端框架采用 TypeScript 和 Eslint，规范代码，提高项目可持续性和可维护性。

### 演示环境

演示地址：[http://skysong.gitee.io/coffee-ui](http://skysong.gitee.io/coffee-ui)

备注：哪里有 Linux 服务器呢？

### 预览

<table>
    <tr>
        <td>
            <img src="./doc/images/project/1.png"/>
        </td>
        <td>
            <img src="./doc/images/project/2.png"/>
        </td>
    </tr>
    <tr>
        <td>
            <img src="./doc/images/project/3.png"/>
        </td>
        <td>
            <img src="./doc/images/project/4.png"/>
        </td>
    </tr>
    <tr>
        <td>
            <img src="./doc/images/project/5.png"/>
        </td>
        <td>
            <img src="./doc/images/project/6.png"/>
        </td>
    </tr>
    <tr>
        <td>
            <img src="./doc/images/project/7.png"/>
        </td>
        <td>
            <img src="./doc/images/project/8.png"/>
        </td>
    </tr>
    <tr>
        <td>
            <img src="./doc/images/project/9.png"/>
        </td>
        <td>
            <img src="./doc/images/project/10.png"/>
        </td>
    </tr>
    <tr>
        <td>
            <img src="./doc/images/project/11.png"/>
        </td>
        <td>
            <img src="./doc/images/project/12.png"/>
        </td>
    </tr>
    <tr>
        <td>
            <img src="./doc/images/project/13.png"/>
        </td>
        <td>
            <img src="./doc/images/project/14.png"/>
        </td>
    </tr>
</table>

### 项目地址

- [coffee-ui](https://gitee.com/skysong/coffee-ui) - 前端 UI
- [coffee-boot](https://gitee.com/skysong/coffee-boot) - 后端 API
- [coffee-cloud](https://gitee.com/skysong/coffee-cloud) - 后端 API（TODO，待完善，有时间再开发）

### 项目结构

```
coffee-boot
├── coffee-admin -- 内置功能，后台管理
├── coffee-codegen -- 内置功能，代码生成
├── coffee-common --内置功能，通用工具
├── coffee-framework -- 内置功能，核心模块
├── coffee-system -- 内置功能，系统模块
├── coffee-oss -- 内置功能，OSS文件存储模块
```

### 核心依赖

| 依赖                   | 版本       |
| ---------------------- | ---------- |
| Spring Boot            | 2.5.8      |
| Sa Token               | 1.29.0     |
| Mybatis Plus           | 3.5.1      |
| Mybatis Plus Generator | 3.4.1      |
| Hutool                 | 5.7.21     |
| Guava                  | 30.1.1-jre |
| EasyExcel              | 3.0.5      |
| Fastjson               | 1.2.79     |
| Minio                  | 8.3.7      |

### 内置功能

```
1、菜单管理
2、字典管理
3、部门管理
4、岗位管理
5、角色管理
6、用户管理
7、参数设置
8、行政区域
9、在线用户
10、操作日志
11、账户设置
```

### 前端安装

```
# 安装依赖
pnpm install

# 运行项目
pnpm serve

# 打包发布
pnpm build

```

### 前端说明

- [vue-vben-admin](https://gitee.com/annsion/vue-vben-admin) - 基于该前端框架开发，不定期同步更新。

### 环境安装

```
1、安装Mysql数据库，安装Redis，安装MinIO文件存储。
2、执行./doc/db/schema.sql，创建数据库。
3、执行./doc/db/coffee.sql，创建数据表和插入基础数据。
```

### Docker 常用命令

```
1、查看镜像
docker images

2、查看所有容器
docker ps -a

3、拉取镜像
docker pull redis:latest

4、参数解释
--name，设置运行的镜像名称
-p，映射端口，虚拟机端口:docker端口
-e，设置环境变量
-v，挂载目录/文件，虚拟机目录/文件:docker目录/文件
--privileged=true，设置特权，比如为mysql获取root权限
-d，守护进程后台运行
-it，启动并运行
--restart=always，在docker服务重启后,自动重启mysql服务,也可以把docker服务作为开机启动，这样mysql就可以跟着开机启动了
--link，设置容器别名
```

### Docker 安装 Redis

```
1、拉取镜像
docker pull redis:latest

2、启动并运行
docker run -itd --name redis -p 6379:6379 redis
```

### Docker 安装 Mysql

```
1、拉取镜像
docker pull mysql:5.7

2、创建目录并授权
mkdir -p /data/docker/mysql/data /data/docker/mysql/conf
chmod -R 755 /data/docker

3、创建配置文件并授权
cd /data/docker/mysql/conf
touch my.cnf
vi /data/docker/mysql/conf/my.cnf
chmod 755 /data/docker/mysql/conf/my.cnf
my.cnf内容如下：
[client]
port=3306
default-character-set=utf8
[mysql]
default-character-set=utf8
[mysqld]
character_set_server=utf8
sql_mode=NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES
lower_case_table_names=1

4、启动并运行
docker run -itd --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 --privileged=true -v /data/docker/mysql/conf/my.cnf:/etc/mysql/my.cnf -v /data/docker/mysql/data:/var/lib/mysql -v /data/docker/mysql/logs:/var/log/mysql mysql:5.7

5、远程连接，无法访问
a）进入容器，docker exec -it containerId /bin/bash
b）进入MySql，mysql -uroot
c）切换数据库，use mysql
d）修改权限，grant all privileges on *.* to 'root'@'%' identified by '123456' with grant option;
e）刷新权限，flush privileges;
```

### Docker 安装 MinIO

```
1、拉取镜像
docker pull minio/minio

2、创建目录并授权
mkdir -p /data/docker/minio/data
chmod -R 755 /data/docker

3、启动并运行
docker run -d -p 9000:9000 -p 9001:9001 --name=minio -v /data/docker/minio/data:/data quay.io/minio/minio server /data/data-{1...4} --console-address ":9001" --address ":9000"
```

### MinIO 文件存储

<table>
    <tr>
        <td>
            <img src="./doc/images/minio/minio1.png"/>
        </td>
        <td>
            <img src="./doc/images/minio/minio2.png"/>
        </td>
    </tr>
</table>

### 部署

```
一、打包命令：指定prod环境，进行打包
    mvn clean package -DskipTests -Pprod
二、启动
    1、Windows环境，运行./doc/bin/run.bat
    2、Linux环境，运行./doc/bin/linux/startup.sh
三、注意事项
    1、Linux执行脚本，需要先授权，chmod +x startup.sh
    2、run.bat或startup.sh，需要和coffee-admin.jar放在同一个目录运行
    3、指定端口，需要修改脚本，并添加server.port参数，示例：--server.port=9090
四、服务器部署目录结构
    /opt
    ├── coffee
    ├──── coffee-ui
    ├──── coffee-boot
    ├────── startup.sh
    ├────── coffee-admin.jar
```

### nginx 配置

```
    location / {
        root   /opt/coffee/coffee-ui;
        try_files $uri $uri/ @router;
        index  index.html index.htm;
    }

    location @router {
        rewrite ^.*$ /index.html last;
    }

    location ^~ /api {
        proxy_pass  http://localhost:9090/api;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header REMOTE-HOST $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
```

### 使用帮助

#### 代码同步方法（示例）

1、fork 代码，fork from https://gitee.com/skysong/coffee-ui <br/> 2、创建自己的分支，进行开发，feature-dev <br/> 3、码云强制同步 <br/> <img src="./doc/images/doc/1.png"/> <br/> 4、合并代码，merge into feature-dev <br/> <img src="./doc/images/doc/2.png"/>

#### 代码生成（CodeGenerator）

```
    /** 按照个人需要，进行修改 */
    public static final String AUTHOR = "Kevin";
    public static final String PROJECT_PATH = "D:\\tempCode";
    public static final String PACKAGE_PARENT = "com.coffee";
    public static final String MODULE_NAME = "system";

    /** 生成SQL脚本的上级菜单的ID，要开发的功能，需要放到XXX菜单下面，请找到XXX菜单的ID */
    public static final String PARENT_MENU_ID = "1406064334403878913";

    /** admin的ID，可以不用修改 */
    public static final String CREATE_BY = "1";
    public static final String UPDATE_BY = "1";

    /** 默认菜单图标，可以不用修改，SQL脚本生成之后，在页面选择图标，进行修改即可 */
    public static final String ICON = "ant-design:unordered-list-outlined";

    // 是否导出excel
    public static final Boolean exportExcel = false;

    public static void main(String[] args) {
        new CodeGenerator().generate(
                "sys_example"
        );
    }
```

#### EasyExcel 使用

```
excel标题宽度
两个字：@ColumnWidth(10)
四个字：@ColumnWidth(15)
```

#### Java 编码规范（Java 开发手册更新至嵩山版）

[Java 开发手册](./doc/java开发手册/阿里巴巴Java开发手册（嵩山版）.pdf)

#### Java 规范

```
1、大道至简

2、IDEA安装Alibaba Java Coding Guidelines插件，编码规约扫描，代码中不要出现警告

```

#### 建表规范

```
1、ID主键，bigint(20)，雪花算法

2、审计字段，create_by、create_time、update_by、update_time

3、表中字段类型，主要采用varchar，万一迁移oracle、sqlserver数据库呢，好兼容
```

#### Vue 编码规范

```
1、全局通用组件放在/src/components目录

2、业务组件放在./components目录

3、文件夹，使用camelCase命名格式

4、*.vue文件使用PascalCase命名格式，index.vue除外

5、属于类的*.js文件，使用PascalCase命名格式，index.js除外

6、属于Api的*.js文件，统一加上Api后缀，使用camelCase命名格式

7、其他类型的*.js文件，使用camelCase命名格式

8、样式文件、图片文件等，使用kebab-case命名格式

9、api模块和view模块，一一对应

10、方便记忆规则和项目开发
```

#### CSS 编码规范

```
CSS编码规范，BEM：就是块（block）、元素（element）、修饰符（modifier）

.block{}
.block__element{}
.block--modifier{}

.block 代表了更高级别的抽象或组件。
.block__element 代表.block的后代，用于形成一个完整的.block的整体。
.block--modifier代表.block的不同状态或不同版本
```

### QQ 群，问题反馈和 Bug 修复

805450144
