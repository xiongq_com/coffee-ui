import { defHttp } from '/@/utils/http/axios';
import { useGlobSetting } from '/@/hooks/setting';
const globSetting = useGlobSetting();

import { downloadByUrl } from '/@/utils/file/download';
import { UploadFileParams } from '/#/axios';

enum Api {
  listDictModel = '/system/sysDict/listDictModel',
  getConfigValue = '/system/sysConfig/getConfigValue',
}

export function listDictModel(params?: object) {
  return defHttp.get({ url: Api.listDictModel, params: params });
}

export function getConfigValue(params?: object) {
  return defHttp.get({ url: Api.getConfigValue, params: params });
}

export function downloadFile(filepath: string) {
  downloadByUrl({
    url: globSetting.apiUrl + '/common/download?filepath=' + filepath,
    target: '_self',
  });
}

export function uploadApi(
  params: UploadFileParams,
  onUploadProgress: (progressEvent: ProgressEvent) => void,
) {
  return defHttp.uploadFile(
    {
      url: globSetting.apiUrl + '/common/upload',
      onUploadProgress,
    },
    params,
  );
}
